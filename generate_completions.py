#import simplejson as json

import json
import os
import pprint
import urllib2
from collections import OrderedDict  

class CloudFormationHelper():
	keywords = {}
	completions = {}
	allowedValues = {}
	datatypes = set()

	def __init__( self, schemaURL = 'https://d3rrggjwfhwld2.cloudfront.net/CloudFormationSchema/CloudFormationV1.schema', schemaFile =  './cloudformation.schema'):
		if not os.path.isfile( schemaFile ):
			#downlad schema
			print 'fetching schema...'
			u = urllib2.urlopen( schemaURL )
			f = open( schemaFile, 'w')
			f.write(u.read())
			f.close()
			print 'fetched schema done'
		
		cfschema = json.loads( open( schemaFile, 'rb').read() )
		print 'cloudformation schema loaded'

		for key, value in cfschema["pseudo-parameters"].items():			
			self.keywords[ key ] = value["description"] 
		print 'pseudo-parameters processed'

		for key, value in cfschema["intrinsic-functions"].items():			
			self.keywords[ key ] = value["description"] 
		print 'intrinsic-functions processed'

		for key, value in cfschema["root-schema-object"]["properties"]["Resources"]["child-schemas"].items():
			self.keywords[ key ] = value["description"] 
		print 'Resources / child schemas processed'
		
		completions = self.findCompletions( cfschema["root-schema-object"] )
	

		"""
		print "root-schema-object.properties.Conditions"
		completions = self.findCompletions( cfschema["root-schema-object"]["properties"]["Conditions"] )
		pprint.pprint( completions )

		print "root-schema-object.properties.Parameters.default-child-schema"
		completions = self.findCompletions( cfschema["root-schema-object"]["properties"]["Parameters"]["default-child-schema"] )
		pprint.pprint( completions )

		print "root-schema-object.properties.Resources.child-schemas"
		completions = self.findCompletions( cfschema["root-schema-object"]["properties"]["Resources"]["child-schemas"] )
		pprint.pprint( completions )
		"""
		
		
		#completions = self.findCompletions( cfschema[ u'root-schema-object'][u'properties']) #[ u'Resources']
	
	def findCompletions( self, pObject ):
		if isinstance( pObject, dict ):
			for key, value in pObject.items():
				
				if pObject.has_key('type'): 
					print "type: ", pObject["type"]
					try: 
						self.datatypes.add( value )
					except Exception, e:
						print e

				if key == "properties":									#extract properties
					for pKey, pValue in pObject[key].items():
						try:
							self.completions[pKey] = pValue["description"]
						except:
							self.completions[pKey] = ''
						print 'property: %s' % pKey
						if isinstance(pValue, dict): self.findCompletions( pValue )
				
				elif key == "allowed-values":
					for item in pObject[key]:
						thiskey = item
						try:
							#print item["value"], item["display-label"]
							thiskey = item["value"]
							self.allowedValues[item["value"]] =  item["display-label"]							
						except:	
							#TODO process arrays not just dicts					
							self.allowedValues[thiskey] = ''
						print 'allowed-value: %s' % thiskey

				if isinstance(value, dict): self.findCompletions( value ) #recurse deeper
		else: return

	def prepareSchema( self ):
		autoCompletions = {}# OrderedDict()
		template = """\n{ 
		        "scope": "source.cloudformation",
		        "completions":
		       	[%s]\n}
		"""
		
		for k,v in self.completions.items(): 
			autoCompletions[ '\n\t\t\t\t"%s",' % (k)] = v
		
		for k,v in self.allowedValues.items(): 
			#self.keywords[k] = v
			autoCompletions[ '\n\t\t\t\t"%s",' % (k)] = v

		for k in self.datatypes: 
			#self.keywords[k] = ''
			autoCompletions[ '\n\t\t\t\t"%s",' % (k)] = k

		for k, v in self.keywords.items():
			autoCompletions[ '\n\t\t\t\t"%s",' % (k)] = v

		pprint.pprint(self.datatypes)

		#completionsJSON = template % autoCompletions
		#f = open('cloudformation.sublime-completions','wb')
		#f.write( completionsJSON.keys() )
		#f.close()
		print 'written autocompletions file'
		#print completionsJSON 



if __name__ == "__main__":
	cfhelper = CloudFormationHelper()
	cfhelper.prepareSchema(  )

