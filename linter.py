#
# linter.py
# Linter for SublimeLinter3, a code checking framework for Sublime Text 3
# License: MIT
#
#REF: http://www.sublimelinter.com/en/latest/usage.html
#REF: http://www.sublimetext.com/docs/3/api_reference.html#sublime.Window
"""
http://www.sublimelinter.com/en/latest/creating_a_linter.html
http://www.sublimelinter.com/en/latest/linter_methods.html
https://github.com/SublimeLinter/SublimeLinter-for-ST2
https://github.com/SublimeLinter/SublimeLinter3
http://sublimelinter.readthedocs.org/en/latest/installation.html
https://github.com/stopdropandrew/SublimeLinter-puppet-lint
http://docs.sublimetext.info/en/latest/reference/completions.html
http://manual.macromates.com/en/scope_selectors#descendant_selectors
"""

"""This module exports the CloudFormation plugin class."""

from SublimeLinter.lint import Linter, util

class CloudFormationLint(Linter):

    """Provides an interface to cloudformation validate."""

    syntax = 'cloudformation'
    cmd = ('aws','cloudformation','validate-template', '--template-body', '*')
    #--template-url ?self.window.active_view()?
    #'%{linenumber}:%{column}:%{kind}:%{message}', 
    #multiline = True
    executable = None
    regex = (
        r'^(?P<line>\d+):(?P<col>\d+):'
        r'((?P<warning>warning)|(?P<error>error)):'
        r'(?P<message>.+)'
    )
    tempfile_suffix = '-'
    error_stream = util.STREAM_STDOUT
    word_re = r'^(".*?"|[-\w]+)'